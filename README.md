# 🐳 docker-image-quarkus-gitpod

This project allows you to build and make a docker image available.

With [Sdkman](https://sdkman.io/), [Jbang](https://www.jbang.dev/) and [Quarkus CLI](https://quarkus.io/), this image will fit you allow projects to be developed using Quarkus on the Cloud IDE [Gitpod].

A first image is available here : `docker pull registry.gitlab.com/jeanphi-baconnais/docker-image-quarkus-gitpod`.

## Issues

It's the first version of this docker image. Issues are created in [this page](https://gitlab.com/jeanphi-baconnais/docker-image-quarkus-gitpod/-/issues) to ameliorate this project.
